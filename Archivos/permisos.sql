
truncate table custosstage.`temp_resource_permissions_task`;
insert into custosstage.`temp_resource_permissions_task` (idaltop, created_at, role,country_id)
select idaltop, created_at, role,country_config_id from person where
idaltop in ({idaltops}) 
and status=1;


###############
-- PERMISOS TASKS
################

insert into resource_permissions (user_id, resource,  can_create,can_read, can_update, can_delete, created) 
 select distinct idaltop, '/accounts', 1 , 1 , 0,0,now()
 from   custosstage.temp_resource_permissions 
 where  idaltop not in (select user_id from resource_permissions where resource='/accounts' );

insert into resource_permissions (user_id, resource, can_create,can_read, can_update, can_delete, created) 
  select distinct idaltop, '/checklists/questions/*',0, 1 , 1 , 1,now()
  from   custosstage.`temp_resource_permissions_task` 
  where  idaltop not in (select user_id from resource_permissions where resource='/checklists/questions/*' ); -- ya existia 
   
   



insert into resource_permissions (user_id, resource, can_create, can_read, can_update,  can_delete, created) 
select distinct idaltop,  '/checklists/templates/*',0, 1 , 1 , 1,now()
from   custosstage.`temp_resource_permissions_task` 
where idaltop not in (select user_id from resource_permissions where resource='/checklists/templates/*' );


INSERT INTO `resource_permissions` (user_id, resource,  can_create, can_read, can_update,  can_delete,  created) 
 select distinct idaltop,'/places', 0,1,0,0,now()
 from    custosstage.`temp_resource_permissions_task` 
where   idaltop not in (select user_id from resource_permissions where resource='/places' );


INSERT INTO `resource_permissions` (user_id, resource,  can_create, can_read, can_update,  can_delete,  created) 
 select distinct idaltop,'/places/*', 0,1,0,0,now()
 from    custosstage.`temp_resource_permissions_task` 
where   idaltop not in (select user_id from resource_permissions where resource='/places/*' );





insert into resource_permissions (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/people',0, 1, 0, 0,now()
 from  custosstage.`temp_resource_permissions_task` 
 where  idaltop not in (select user_id from resource_permissions where resource='/people' );


insert into resource_permissions (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/people/*',0, 1, 0, 0,now()
 from   custosstage.`temp_resource_permissions_task` 
 where idaltop not in (select user_id from resource_permissions where resource='/people/*' );
 
 insert into resource_permissions (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/private/files',1, 0, 0, 0,now()
 from   custosstage.`temp_resource_permissions_task` 
 where idaltop not in (select user_id from resource_permissions where resource='/private/files' );
 

INSERT INTO `resource_permissions` (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/stores/*', 0,1,0,0,now()
 from    custosstage.`temp_resource_permissions_task` 
where   idaltop not in (select user_id from resource_permissions where resource='/stores/*' );



INSERT INTO `resource_permissions` (`user_id`, `resource`,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/stores',0,1,0,0,now()
 from   custosstage.`temp_resource_permissions_task` 
where idaltop not in (select user_id from resource_permissions where resource='/stores' );



INSERT INTO `resource_permissions` (`user_id`, `resource`,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/task_process',1,1,1,1,now()
 from   custosstage.`temp_resource_permissions_task` 
where idaltop not in (select user_id from resource_permissions where resource='/task_process' );

INSERT INTO `resource_permissions` (`user_id`, `resource`,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/task_process/*',1,1,1,1,now()
 from   custosstage.`temp_resource_permissions_task` 
where idaltop not in (select user_id from resource_permissions where resource='/task_process/*' );

insert into resource_permissions (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/task_process/tags',1, 0, 0, 0,now()
 from  custosstage.`temp_resource_permissions_task` 
 where  idaltop not in (select user_id from resource_permissions where resource='/task_process/tags' );

  
  INSERT INTO `resource_permissions` (`user_id`, `resource`,can_create, can_read, can_update,  can_delete,created) 
 select distinct idaltop,'/todolist/templates/*',0,1,1, 1,now()
 from   custosstage.`temp_resource_permissions_task` 
 where  idaltop not in (select user_id from resource_permissions where resource='/todolist/templates/*' );-- ya existia





insert into resource_permissions (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/tasks_requests',1, 1, 1, 0,now()
 from   custosstage.`temp_resource_permissions_task` 
 where idaltop not in (select user_id from resource_permissions where resource='/tasks_requests' );

insert into resource_permissions (user_id, resource,  can_create, can_read, can_update,  can_delete, created) 
 select distinct idaltop,'/tasks_requests/*',1, 1, 0, 1,now()
 from   custosstage.`temp_resource_permissions_task` 
 where idaltop not in (select user_id from resource_permissions where resource='/tasks_requests/*' );
 