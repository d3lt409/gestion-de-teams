import sys
sys.path.append(".")

from tkinter import Tk,Canvas,Label,Button
from os import system
import platform

class main_team:

    def __init__(self) -> None:
        self.root= Tk()
        self.root.title("Teams")
        self.root.geometry('350x350+500+150')
        
        self.canvas()
        self.label()
        self.crear_team()
        self.agreagar_user_team()
        self.buscar_team()
        self.buscar_user_team()
        self.cancel_button()

        self.root.mainloop()
    
    def canvas(self):
        self.canvas1 = Canvas(self.root, width = 350, height = 350, bg = 'lightsteelblue2', relief = 'raised')
        self.canvas1.pack()

    def label(self):
        self.label1 = Label(self.root, text='¿Qué quieres hacer?', bg = 'lightsteelblue2')
        self.label1.config(font=('helvetica', 20))
        self.canvas1.create_window(180, 60, window=self.label1)


    def update_button(self,val,t):
        self.text.set(f"      {val}     ")
        self.root.after(1000, lambda:self.countdown(t-1))

    def crear_team(self):
        self.button_crear = Button(text="      Crear Team     ", command=shutdown_pc, bg='green', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 140, window=self.button_crear)

    def agreagar_user_team(self):
        self.button_agregar = Button(text="      Agregar usuarios al Team     ", command=shutdown_pc, bg='green', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 180, window=self.button_agregar)

    def buscar_team(self):
        self.button_buscar = Button(text="      Buscar Team     ", command=shutdown_pc, bg='green', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 220, window=self.button_buscar)

    def buscar_user_team(self):
        self.button_buscar_user = Button(text="      Buscar usuario del Team     ", command=shutdown_pc, bg='green', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 260, window=self.button_buscar_user)

    def cancel_button(self):
        self.exitButton = Button(self.root, text='       Cancelar     ',command=no_shutdown_pc, bg='brown', fg='white', font=('helvetica', 12, 'bold'))
        self.canvas1.create_window(180, 300, window=self.exitButton)


def shutdown_pc ():
    sistema = platform.system()
    if sistema == "Windows":
        system("""shutdown /s /t 10 /c "Trate de cerrar todo, tiene 10 segundos" """)
        exit()
    else:
        system("""shutdown -P +1 "Trate de cerrar todo, tiene 60 segundos" """)
        exit()

def no_shutdown_pc ():
    exit()
    
main_team()