# Gestión de teams #

Hace la gestión de CRUD para los teams, así como asígnar persinas al team y eliminar personas 
a un team

## Credenciales ##

* En la carpeta de Credenciales hay un archivo llamado Ambientes.json, deben cambiar el parametro AUTH para ambos ambientes con el usuario personal
- El valor "1" es para Produccion
- El valor "2" es para Desarrollo

* Para los ambienes de produccion y desarrollo en la base de datos. Se debe abrir la carpeta Credenciales y en cada ambiente configurar con los puertos que tiene corriendo para cada uno.

### Uso ###

* Todo los de gestión está en el archivo Gestion_teams.py

### Tener en cuenta ###

* La apliación fallará si cambia algo dentro de lo que está configurado, o si no tiene la base de datos corriendo