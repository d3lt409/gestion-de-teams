from typing import Any
import mysql.connector
from mysql.connector.connection import MySQLConnection
from mysql.connector.cursor import MySQLCursor


def iniciarConexion(ambiente):
    """
        Inicia la conexión con la base de Datos indicada:
            - Debe existir el archivo "credenciales" del ambiente a usar:
                port
                host
                user
                password

        Ambos archivos se deben agregar al .gitignore
    """

    archivo = 'Credenciales/credenciales' + ambiente + '.txt'
    credenciales = open(archivo, 'r')

    datos = credenciales.readlines()
    host = datos[0].strip()
    port = datos[1].strip()
    user = datos[2].strip()
    pawd = datos[3].strip()
    db:MySQLConnection = mysql.connector.connect(
        host=host,
        port=port,
        user=user,
        passwd=pawd,
        database='custos'
    )

    mycursor:MySQLCursor = db.cursor()
    return db,mycursor

def ejecutarConsulta(consulta: str,mycursor:MySQLCursor) -> (list):
    """
        Ejecuta una consulta SELECT en la Base de Datos
    """
    mycursor.execute(consulta)

    return mycursor.fetchall()

def ejecutarConsultaUnica(consulta: str,mycursor:MySQLCursor) -> (Any):
    """
        Ejecuta una consulta SELECT en la Base de Datos
    """
    mycursor.execute(consulta)

    return mycursor.fetchone()

def ejecutarCambio(consulta: str,mycursor):
    """
        Ejecuta una consulta que puede CAMBIAR la Base de Datos (ALTER, INSERT)
    """
    mycursor.execute(consulta)

def InsertarMuchos(consulta: str, elementos: list,mycursor):

    values =  "%s,"*len(elementos[0])
    values = values[:len(values)-1]
    values = f" values ({values})"

    mycursor.executemany(consulta+values,elementos)

def cerrarConexion(db,mycursor):
    """
        Cierra la conexión con la Base de Datos
    """
    mycursor.close()
    db.commit()
    db.close()