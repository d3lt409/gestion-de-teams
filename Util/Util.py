import numpy as np
import re
from unicodedata import normalize

@np.vectorize
def setPais(pais: str) -> (int):
    """
        Devuelve los datos del pais respectivo
    """

    switchPais = {
        "USA": 1,
        "Chile": 2,
        "Colombia": 3,
        "México": 4,
        "España": 5,
        "Demo US": 6,
        "Demo ES": 7
    }

    return switchPais.get(pais)

@np.vectorize
def ponerComillas(dato: str) -> (str):

    if dato != "null":

        if "'" in dato:
            dato = '"' + dato + '"'
        else:
            dato = "'" + dato + "'"

    return dato

@np.vectorize
def concatValores(*args, separador: str):

    consulta = ""

    for i in args:
        consulta += str(i) + separador

    return consulta[:-len(separador)]

@np.vectorize
def capitalizarDatos(dato: str) -> (str):
    """
        Devuelve un dato de entrada capitalizado y sin espacios a los lados
    """
    return str(dato).title().strip()
    
def listarDatos(lista: list, inicio: str="", fin: str="") -> (str):
    """
        Devuelve los datos de una lista en forma de string listado
    """
    lista = [str(i) for i in lista]
    return (f'{inicio}{f"{fin}{inicio}".join(lista)}')


def normalizar(value:str)->str:
    s = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", 
        normalize( "NFD", value), 0, re.I)
    # -> NFC
    s = normalize( 'NFC', s)

    return s.replace("ñ","n").lower()

