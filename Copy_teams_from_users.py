
from datetime import datetime
import sys
from mysql.connector.cursor import MySQLCursor
sys.path.append(".")
import pandas as pd
import requests
import re
import Util.Db as Db
import Util.Util as Util
import json
import urllib.parse
import os
import time

amb_json = open("Ambientes.json", "r")
AMB = json.load(amb_json)
amb_json.close()
EXPIRED = "The token has expired"
NO_SABE = "No se sabe"
INGRESO_VALIDO = "             \nIngrese valor válido\n"
FORMATO_USUARIOS = "\n¿Ya llenó el formato de excel 'UsuariosTeams.xlsx'? (s/n) > "
INGRESO_CUENTA = "Ingresa el nombre de la cuenta > "
PRESIONE = "\nPresione enter para continuar..."
INGRESE_TEAM_ID = "Ingrese el team_id >"
CAMBIO = {"1": "name", "2": "country", "3": "account"}
PATH = sys.path[0]
db_prod, mycursor_prod = Db.iniciarConexion(AMB["1"]['name'])
#db_dev, mycursor_dev = Db.iniciarConexion(AMB["2"]['name'])

def change_token():
    response = requests.post(f"{AMB['1']['URL']}/authenticate",
                             headers=AMB['1']['HEADERS'],
                             json=AMB['1']["AUTH"])
    if response.ok:
        AMB['1']['HEADERS']['Authorization'] = response.json()['auth']['token']
        json.dump(AMB, open("Ambientes.json", "w"))
    else:
        print(response.text)
        sys.exit()

def getTeamUser(user):

    if (type(user) == str):
        
        q = f"Select idaltop from custos.person where email ='{user}' and status = 1"
        res = Db.ejecutarConsultaUnica(q, mycursor_prod)
        if not res:
            print(f"No existe el usuario {user}")
            sys.exit()
        user = res[0]
    page = f"{AMB['1']['URL']}/users/{str(user)}/teams"
    response = requests.get(page, headers=AMB['1']["HEADERS"])
    if (response.ok):
        if (len(response.json())>0):
            return [(val["team_id"],val["roles"][0]["role"]) for val in response.json()]
        else:
            return []
    elif (response.json() == []):
        print(f"NO se encontró usuarios")
    elif("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token()
        getTeamUser(user)
    else:
        print(NO_SABE, response.text)

def buscarUsuarios(data: pd.DataFrame, idaltop: bool, country):
    
    if (idaltop):
        idaltop_lista = Util.listarDatos(data["idaltop"], "'", "',")
        idaltop_lista += "'"
        q = f"Select idaltop,email from custos.person where idaltop in ({idaltop_lista}) and country_config_id = {country} and status = 1 and person_type_id is not null and idalto_rol is not null;"
        res = Db.ejecutarConsulta(q, mycursor_prod)
        df_email = pd.DataFrame(res, columns=["idaltop", "Usuario"])
        df = pd.DataFrame(data["idaltop"])
        df["check"] = df["idaltop"].isin(df_email["idaltop"])
        df_email_bad = df.loc[~df["check"]]
        if (len(df_email_bad) > 0):
            print(
                f"Los usuario no existen para el país {country} o están inactivos")
            print(df_email_bad[["idaltop"]])
            sys.exit()
        data = pd.merge(data, df_email, on=["idaltop"], how="inner")

    else:
        data["Usuario_nuevo"] = data["Usuario_nuevo"].apply(lambda row: row.strip().lower())
        email_list = Util.listarDatos(data["Usuario_nuevo"], "'", "',")
        email_list += "'"
        q = f"Select idaltop,email from custos.person where email in ({email_list}) and country_config_id = {country} and status = 1 and person_type_id is not null and idalto_rol is not null"
        res = Db.ejecutarConsulta(q, mycursor_prod)
        df_email = pd.DataFrame(res, columns=["idaltop_usuario_nuevo", "Usuario_nuevo"])
        df = pd.DataFrame(data["Usuario_nuevo"])
        df["check"] = df["Usuario_nuevo"].isin(df_email["Usuario_nuevo"])
        df_email_bad = df.loc[~df["check"]]
        if (len(df_email_bad) > 0):
            print(
                "Los usuario no existen en el pasí o están inactivos o posiblemente no están creados")
            print(df_email_bad[["Usuario_nuevo"]])
            sys.exit()
        data = pd.merge(data, df_email, on=["Usuario_nuevo"], how="inner")
    return data

def add_user(team_id, user, rol: str, num_index):

    body = {
        "$op": "add",
        "teams": [
            {"team_id": team_id,
             "roles": [{
                 "name_modulo": "checklist",
                 "role": rol.lower()
             }]
             }
        ]
    }
    page = f"{AMB['1']['URL']}/users/{user}/teams"
    response = requests.patch(page, headers=AMB['1']['HEADERS'], json=body)
    print(response.text)

    if (response.ok):
        print(f"\n {num_index+1} Se ingresa el usuario {user} al team {team_id}\n")
    elif ("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token()
        add_user(team_id, user, rol, num_index)
    else:
        print(f"\n No se ingresa el usuario {user} \n")

def asignar_permisos_task(data:list,mycursor:MySQLCursor):
    sql = open("teams/permisos.sql","r")
    ids = ",".join(data)
    file_sql = sql.read().replace("{idaltops}",ids)
    mycursor.execute(file_sql,multi=True)
    db_prod.commit()
    sql.close()
    
def deleteTeamUser(idaltop, team_id):
    """
        Elimina un usuario del team indicado
    """
    url = f"{AMB['1']['URL']}/users/{str(idaltop)}/teams"
    body = {
        "$op": "remove",
        "team_id": team_id
    }

    response = requests.patch(url, json=body, headers=AMB['1']['HEADERS'])

    if (response.ok):
        print(f"Se elimina el usuario {idaltop} del team {team_id}")
    else:
        print(f"No se elimina el usuario {idaltop}")


def search_teams_from_users():
    df = pd.read_excel("teams/Change_user_team.xlsx")
    df = df.drop_duplicates()
    df.reset_index(inplace=True,drop=True)
    df["Usuario_nuevo"] = df["Usuario"].str.replace("@","+audit@")
    df = buscarUsuarios(df,False,1)
    mycursor_prod.executemany( 
        "INSERT INTO custos.resource_permissions (user_id, resource, can_read, created) values (%s,%s,%s,%s)",
        [(val,"/discovery","1",datetime.now()) for val in df["idaltop_usuario_nuevo"].astype(str).tolist()])
    db_prod.commit()
    # mycursor_prod.executemany(
    #     "insert ignore into people_accounts values (%s,%s,%s,%s,%s,%s,%s)",[(val,"5851","1","31735",datetime.now(),"31735",datetime.now()) for val in df["idaltop_usuario_nuevo"].astype(str).tolist()])
    # for i,row in df.iterrows():
    #     for tup in getTeamUser(row["Usuario"]):
    #         deleteTeamUser(row['idaltop_usuario_nuevo'],tup[0])
    #asignar_permisos_task(df["idaltop_usuario_nuevo"].astype(str),mycursor_prod)
    

search_teams_from_users()
Db.cerrarConexion(db_prod,mycursor_prod)