
import sys
from typing import Tuple
from mysql.connector.errors import InterfaceError
from mysql.connector.cursor import MySQLCursor
import numpy as np
sys.path.append(".")
import pandas as pd
import requests
import re
import Util.Db as Db
import Util.Util as Util
import json
import urllib.parse
import os
import time



amb_json = open("Credenciales/Ambientes.json", "r")
AMB = json.load(amb_json)
amb_json.close()
EXPIRED = "The token has expired"
NO_SABE = "No se sabe"
INGRESO_VALIDO = "             \nIngrese valor válido\n"
FORMATO_USUARIOS = "\n¿Ya llenó el formato de excel 'UsuariosTeams.xlsx'? (s/n) > "
INGRESO_CUENTA = "Ingresa el nombre de la cuenta > "
PRESIONE = "\nPresione enter para continuar..."
INGRESE_TEAM_ID = "Ingrese el team_id >"
CAMBIO = {"1": "name", "2": "country", "3": "account"}
PATH = sys.path[0]
CONEXION_INVALIDA = "Por favor, haga la conexión a la base de datos indicada"


def change_token(amb):
    response = requests.post(f"{AMB[amb]['URL']}/authenticate",
                             headers=AMB[amb]['HEADERS'],
                             json=AMB[amb]["AUTH"])
    if response.ok:
        AMB[amb]['HEADERS']['Authorization'] = response.json()['auth']['token']
        json.dump(AMB, open("Ambientes.json", "w"))
    else:
        print(response.text)
        sys.exit()

def buscarName(name, account, mycursor,amb):
    if (amb == "2" or amb == 2):
        return False
    
    con = f"select * from custosstage.team_accounts where name like '%{name}%' and account = {account}"
    res = Db.ejecutarConsultaUnica(con, mycursor)
    if res:
        return True
    return False

def buscarTeam(team_id, amb):
    page = f"{AMB[amb]['URL']}/teams/{team_id}"
    while True:
        try:
            time.sleep(3)
            response = requests.get(page, headers=AMB[amb]["HEADERS"])
            if (response.ok):
                return response.json()
            if (response.status_code == 404):
                return None
            elif(("error" in response.json() and response.json()["error"] == EXPIRED)):
                change_token(amb)
                buscarTeam(team_id, amb)
        except (requests.exceptions.SSLError) as _:
            time.sleep(2)
            continue


def insertaTeam(team_id, name, country, account, amb,mycursor):
    payload = {
        "team_id": team_id,
        "name": name,
        "country": country,
        "account": account
    }

    response = requests.post(
        f"{AMB[amb]['URL']}/teams", headers=AMB[amb]["HEADERS"], json=payload)
    if ("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token(amb=amb)
        insertaTeam(team_id, name, country, account, amb,mycursor)
    # if (response.ok and amb == "1"):
        
    #     Db.ejecutarCambio(
    #         f"INSERT INTO custosstage.team_accounts values('{team_id}','{name}',{account})", mycursor)
    #     print(f"Se crea el team {payload}")
    #     return
    print(response.text)


def cambiar_value_team(team_id, campo, new_value, amb):
    page = f"{AMB[amb]['URL']}/teams/{team_id}"
    body = {CAMBIO[campo]: new_value}
    en = input(
        f"\n\t{body}\n\t¿Está seguro que desea hacer el cambio? (s/n) >")
    if (en != "s"):
        return
    response = requests.put(page, headers=AMB[amb]["HEADERS"], json=body)
    if(response.ok):
        print(f"Se cambia el {CAMBIO[campo]} del team {team_id} a {new_value}")
    elif("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token(amb)
        cambiar_value_team(team_id, campo, new_value, amb)
    else:
        print(f"No se pudo cambiar el {CAMBIO[campo]} al team {team_id}")


def getTeam_with_name(amb, team_name: str,account_id = None):
    page = f"{AMB[amb]['URL']}/teams?page[number]=1"
    response = requests.get(page, headers=AMB[amb]["HEADERS"])
    if (response.ok):
        
        total_pages = response.json()["pagination"]["total_pages"]
        lista = []
        for team in response.json()["list"]:
            try:
                if (account_id):
                    if (int(team["account"]) == account_id):
                        re.search(team_name.upper(), team["name"].upper()).group(0)
                        lista.append(team)
                else:
                    re.search(team_name.upper(), team["name"].upper()).group(0)
                    lista.append(team)
                
            except Exception as _:
                continue
        for i in range(2, total_pages+1):
            page = f"{AMB[amb]['URL']}/teams?page[number]={i}"
            response = requests.get(page, headers=AMB[amb]["HEADERS"])
            if (response.ok):
                # print(response.json()["list"])
                for team in response.json()["list"]:
                    try:
                        if (account_id):
                            if (int(team["account"]) == account_id):
                                re.search(team_name.upper(), team["name"].upper()).group(0)
                                lista.append(team)
                        else:
                            re.search(team_name.upper(), team["name"].upper()).group(0)
                            lista.append(team)
                    except Exception as _:
                        continue
            else:
                print("Algo pasó compita :/")
                sys.exit()
        return lista

    if (response.status_code == 404):
        return False
    elif("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token(amb)
        getTeam_with_name(amb, team_name)


def getTeam_by_account(amb, account_id):
    page = f"{AMB[amb]['URL']}/teams?page[number]=1"
    response = requests.get(page, headers=AMB[amb]["HEADERS"])
    if (response.ok):
        total_pages = response.json()["pagination"]["total_pages"]
        lista = []
        for team in response.json()["list"]:
            try:
                if account_id == team["account"]:
                    lista.append(team)
            except KeyError as _:
                continue
        for i in range(2, total_pages+1):
            page = f"{AMB[amb]['URL']}/teams?page[number]={i}"
            response = requests.get(page, headers=AMB[amb]["HEADERS"])
            if (response.ok):
                # print(response.json()["list"])
                for team in response.json()["list"]:
                    try:
                        if account_id == team["account"]:
                            lista.append(team)
                    except KeyError as _:
                        continue
            else:
                print("Algo pasó compita :/")
                sys.exit()
        return lista
    
    if (response.status_code == 404):
        return False
    elif(response.status_code == 401 or (
        "error" in response.json() and response.json()["error"] == EXPIRED)):
        change_token(amb)
        getTeam_with_name(amb, account_id)

@np.vectorize
def deleteTeam(amb, team_id):
    if (" " in team_id):
        team_id = urllib.parse.quote(team_id)
    page = f"{AMB[amb]['URL']}/teams/{team_id}/users"
    response = requests.get(page, headers=AMB[amb]['HEADERS'])
    if (response.ok):
        users = response.json()["list"]
        for res in users:
            url = f"{AMB[amb]['URL']}/users/{str(res['id'])}/teams"
            body = {
                "$op": "remove",
                "team_id": team_id
            }
            response = requests.patch(url, json=body, headers=AMB[amb]['HEADERS'])
        page = f"{AMB[amb]['URL']}/teams/{team_id}/users"
        response = requests.get(page, headers=AMB[amb]['HEADERS'])
        if (len(response.json()["list"]) == 0):
            print("El team quedó eliminado")
            return
        print("El team no quedó eliminado (hay usuarios por eliminar")




def getUsersTeam(amb, team_id, email=""):
    page = f"{AMB[amb]['URL']}/teams/{team_id}/users"
    response = requests.get(page, headers=AMB[amb]["HEADERS"])
    if (response.ok):
        print(response.json()["list"])
        if (email != ""):
            for val in response.json()["list"]:
                if (val["email"] == email):
                    print(json.dumps(val, indent=5))
                    return
            print("No está compa")
            return
        for val in response.json()["list"]:
            print(val["id"], val["email"], val["roles"][0]["role"], sep="\t\t")
        print(f"\nLa cantidad de usuarios son {len(response.json()['list'])}")
    elif (response.json() == []):
        print(f"NO se encontró usuarios")
    elif("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token(amb)
        deleteTeam(team_id, amb)
    else:
        print(NO_SABE, response.text)


def getTeamUser(amb, user,mycursor):

    if (type(user) == str):
        
        q = f"Select idaltop from custos.person where email ='{user}' and status = 1"
        res = Db.ejecutarConsultaUnica(q, mycursor)
        if not res:
            print(f"No existe el usuario {user}")
            sys.exit()
        user = res[0]
    page = f"{AMB[amb]['URL']}/users/{str(user)}/teams"
    response = requests.get(page, headers=AMB[amb]["HEADERS"])
    if (response.ok):
        print(json.dumps(response.json(), indent=5))
    elif (response.json() == []):
        print(f"NO se encontró usuarios")
    elif("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token(amb)
        getTeamUser(amb, user,mycursor)
    else:
        print(NO_SABE, response.text)


def deleteTeamUser(amb, team_id, country,mycursor):
    """
        Elimina un usuario del team indicado
    """

    archi = f"{PATH}/Archivos/DeleteUsersTeam.xlsx"
    data = pd.read_excel(archi,engine='openpyxl')
    data.columns = ["idaltop"]
    try:
        for i in data["idaltop"]:
            int(i)
        data = buscarUsuarios(data, True, country, amb,mycursor)
    except ValueError:
        data.columns = ["email"]
        data = buscarUsuarios(data, False, country, amb,mycursor)
    print("los usuarios a eliminar son:\n")
    print(data)
    en = input("\n¿Desea continuar? (s/n)")
    if (en != "s"):
        return
    for i, row in data.iterrows():
        url = f"{AMB[amb]['URL']}/users/{str(row['idaltop'])}/teams"
        body = {
            "$op": "remove",
            "team_id": team_id
        }

        response = requests.patch(url, json=body, headers=AMB[amb]['HEADERS'])

        if (response.ok):
            print(f"{i+1}. Se elimina el usuario {row['email']}")
        else:
            print(f"No se elimina el usuario {row['email']}")


def search_user_Team(data: pd.DataFrame, amb, team_id="") -> pd.DataFrame:

    if (team_id != ""):
        page = f"{AMB[amb]['URL']}/teams/{team_id}/users"
        response = requests.get(page, headers=AMB[amb]['HEADERS'])
        if ("error" in response.json() and response.json()["error"] == EXPIRED):
            change_token(amb)
            search_user_Team(data, amb, team_id)
        if(len(response.json().get("list")) == 0):
            return data
        if (response.json().get("pagination").get("total_items") == 50):
            print(
                f"El team {team_id} ya tiene los 50 usuario, no se puede agregar más")
            sys.exit()
        df = pd.DataFrame(response.json().get("list"))
        #print(json.dumps(response.json().get("list"), indent=5))
        # for val in df["roles"]:
        #     val[0] = val[0].get("role")
        df["roles"] = [val[0].get("role") for val in df["roles"]]
        df_bad = pd.DataFrame(data["idaltop"])
        df_bad["check"] = df_bad["idaltop"].isin(df["id"])
        df_no_in = df_bad.loc[-df_bad["check"]]
        df_in = df_bad.loc[df_bad["check"]]
        if len(df_no_in) == 0:
            df_total = pd.merge(df_in, data, on=["idaltop"], how="inner")
            df_total = pd.merge(df_total, df, on=["email"], how="inner")
            print(f"\n    Todos los usuarios ya pertenecen al team {team_id} ")
            df_total.drop(columns=["check"], inplace=True)
            print(df_total[["id", "email", "roles"]])
            sys.exit()
        elif len(df_in) == 0:
            data = pd.merge(df_no_in, data, on=["idaltop"], how="inner")
            data.drop(columns=["check"], inplace=True)
            return data
        else:
            df_in = pd.merge(df_in, data, on=["idaltop"], how="inner")
            df_in.drop(columns=["check"], inplace=True)
            print(f"\n    Los usuarios ya pertenecen al team {team_id} ")
            print(df_in)

            data = pd.merge(df_no_in, data, on=["idaltop"], how="inner")
            data.drop(columns=["check"], inplace=True)
            return data

    data_not_in = pd.DataFrame()
    teams = data.drop_duplicates(subset=["team_id"])
    for team in teams["team_id"]:
        page = f"{AMB[amb]['URL']}/teams/{team}/users"
        response = requests.get(page, headers=AMB[amb]['HEADERS'])
        new_data = data.loc[data["team_id"] == team]
        if ("error" in response.json() and response.json()["error"] == EXPIRED):
            change_token(amb)
            search_user_Team(data, amb, team_id)
        if(len(response.json().get("list")) == 0):
            data_not_in = data_not_in.append(new_data, ignore_index=True)
            continue
        if (response.json().get("pagination").get("total_items") == 50):
            print(
                f"El team {team} ya tiene los 50 usuario, no se puede agregar más")
            sys.exit()

        df = pd.DataFrame(response.json().get("list"))
        #print(json.dumps(response.json().get("list"), indent=5))
        for val in df["roles"]:
            val[0] = val[0].get("role")
        df_bad = pd.DataFrame(new_data["idaltop"])
        df_bad["check"] = df_bad["idaltop"].isin(df["id"])
        df_no_in = df_bad.loc[-df_bad["check"]]
        df_in = df_bad.loc[df_bad["check"]]

        if len(df_no_in) == 0:
            print(df[["id", "email", "roles"]])
            df_total = pd.merge(df_in, new_data, on=["idaltop"], how="inner")
            df_total = pd.merge(df_total, df, on=["email"], how="inner")
            print(f"\n    Todos los usuarios ya pertenecen al team {team_id} ")
            df_total.drop(columns=["check"], inplace=True)
            print(df_total)  # [["email", "rol"]])
            # sys.exit()
        elif len(df_in) == 0:
            new_data = pd.merge(df_no_in, new_data, on=[
                                "idaltop"], how="inner")
            new_data.drop(columns=["check"], inplace=True)
            data_not_in = data_not_in.append(new_data, ignore_index=True)
        else:
            df_in = pd.merge(df_in, new_data, on=["idaltop"], how="inner")
            df_in.drop(columns=["check"], inplace=True)
            df_in_new = pd.merge(df_in, df, on=["email"], how="inner")
            print(f"\n    Los usuarios ya pertenecen al team {team_id} ")
            print(df_in_new)  # [["email", "rol"]])

            new_data = pd.merge(df_no_in, new_data, on=[
                                "idaltop"], how="inner")
            new_data.drop(columns=["check"], inplace=True)
            data_not_in = data_not_in.append(new_data, ignore_index=True)

        if (response.json().get("pagination").get("total_items")+len(data_not_in) > 50):
            print(
                f"\n\tEl número de usuarios sobrepasa los 50 usuarios para el team {team}")
            data_not_in = data_not_in.reset_index(drop=True)
            print(data_not_in[["email", "rol"]])
            print("\n\tUsuarios en el team:", response.json().get("pagination").get(
                "total_items"), "Usuarios a ingresar:", len(data_not_in))
            sys.exit()
    data_not_in = data_not_in.reset_index(drop=True)
    return data_not_in


def search_user_Account(data: pd.DataFrame, amb, mycursor,accoun_id=0):
    
    if (accoun_id != 0):
        users_list = Util.listarDatos(data["idaltop"], fin=",")
        con = f"select person_id from custos.people_accounts where account_id = {accoun_id} and person_id in ({users_list})"
        res = Db.ejecutarConsulta(con, mycursor)
        res2 = []
        for val in res:
            res2.append(val[0])
        df_users = pd.DataFrame(data["idaltop"])
        df_users.columns = ["idaltop"]
        df_users["check"] = df_users["idaltop"].isin(res2)
        df_users_bad = df_users.loc[~df_users["check"]]
        if (len(df_users_bad) > 0):
            df_users_bad = pd.merge(df_users_bad, data, on=[
                                    "idaltop"], how="inner")
            print(
                f"los siguientes usuarios no pertenecen a la cuenta {accoun_id}")
            print(df_users_bad[["idaltop", "email"]])
            r = input(
                f"\n ¿Desea agregarlos a la cuenta {accoun_id} ? (s,n)\n")
            if (r == "s"):
                print("\n   Se procede a agregar estos usuarios...\n")
                for val in df_users_bad["idaltop"].values:
                    try:
                        q = f"INSERT INTO custos.people_accounts values ({val},{accoun_id},1,31735,now(),31735,now());"
                        Db.ejecutarCambio(q, mycursor)
                    except Exception as e:
                        print(e)
                print("     se agreron los usuarios")
            else:
                sys.exit()
    else:
        data["account_person"] = [Db.ejecutarConsulta(
            f"select person_id from custos.people_accounts where account_id = {row['account_id']} and person_id = {row['idaltop']}", mycursor) for _,row in data.iterrows()]
        data_bad = data.loc[data["account_person"].isna()]
        data.to_excel("Archivos/ejemplo.xlsx")
        print(data_bad)


def add_user(team_id, user, rol: str, num_index, amb):

    body = {
        "$op": "add",
        "teams": [
            {"team_id": team_id,
             "roles": [{
                 "name_modulo": "checklist",
                 "role": rol.lower()
             }]
             }
        ]
    }
    page = f"{AMB[amb]['URL']}/users/{user}/teams"
    response = requests.patch(page, headers=AMB[amb]['HEADERS'], json=body)
    print(response.text, page)
    if (response.ok):
        print(f"\n {num_index+1} Se ingresa el usuario {user} al team {team_id}\n")
    elif (response.status_code == 404):
        print("error de URL")
        exit()
    elif ("error" in response.json() and response.json()["error"] == EXPIRED):
        change_token(amb)
        add_user(team_id, user, rol, num_index, amb)
    else:
        print(f"\n No se ingresa el usuario {user} \n")


def buscarUsuarios(data: pd.DataFrame, idaltop: bool, country, amb,mycursor):
    
    if (idaltop):
        idaltop_lista = Util.listarDatos(data["idaltop"], "'", "',")
        idaltop_lista += "'"
        q = f"Select idaltop,email from custos.person where idaltop in ({idaltop_lista}) and status = 1 and person_type_id is not null and idalto_rol is not null;"
        print(q)
        res = Db.ejecutarConsulta(q, mycursor)
        print(res)
        df_email = pd.DataFrame(res, columns=["idaltop", "email"])
        df = pd.DataFrame(data["idaltop"])
        df["check"] = df["idaltop"].isin(df_email["idaltop"])
        df_email_bad = df.loc[~df["check"]]
        if (len(df_email_bad) > 0):
            print(
                f"Los usuario no existen para el país {country} o están inactivos")
            print(df_email_bad[["idaltop"]])
            sys.exit()
        data = pd.merge(data, df_email, on=["idaltop"], how="inner")

    else:
        data["email"] = data["email"].apply(lambda row: row.strip().lower())
        duplicados = data[data.duplicated("email",keep=False)]
        if (len(duplicados)>0):
            print("Hay usuarios duplicados en el archivo")
            print(duplicados)
            exit()
        
        email_list = Util.listarDatos(data["email"], "'", "',")
        email_list += "'"
        q = f"Select idaltop,email from custos.person where email in ({email_list}) and status = 1 and person_type_id is not null and idalto_rol is not null"
        res = Db.ejecutarConsulta(q, mycursor)
        df_email = pd.DataFrame(res, columns=["idaltop", "email"])
        df = pd.DataFrame(data["email"])
        df["check"] = df["email"].isin(df_email["email"])
        df_email_bad = df.loc[~df["check"]]
        if (len(df_email_bad) > 0):
            print(
                "Los usuario no existen en el pasí o están inactivos o posiblemente no están creados")
            print(df_email_bad[["email"]])
            sys.exit()
        data = pd.merge(data, df_email, on=["email"], how="inner")
        
    data["idaltop"] = list(map(int,data["idaltop"]))   
    return data


def insertarUsers(account_id, country, amb, mycursor, team_id=""):

    data = pd.read_excel(f"{PATH}/Archivos/UsuariosTeams.xlsx",engine='openpyxl')
    data.columns = ["idaltop", "rol", "team_id"]
    data.dropna(0,'all',inplace=True)
    if (len(data) > 50):
        print("     \nCompa tiene más de 50 usuarios a agregar, no se puede :p\n")
        sys.exit()
    try:
        for i in data["idaltop"]:
            int(i)
        data = buscarUsuarios(data, True, country, amb,mycursor)
    except ValueError:
        data.columns = ["email", "rol", "team_id"]
        data = buscarUsuarios(data, False, country, amb,mycursor)

    buscarCuenta(account_id, country, mycursor)
    search_user_Account(accoun_id=account_id, data=data, amb=amb,mycursor=mycursor)
    data = search_user_Team(data, amb, team_id=team_id)
    print(f"\n    Los siguientes usuario no están en el team {team_id}")
    print(data)
    en = input("Desea continuar? ")
    if (en != "s"):
        sys.exit()
    for i, row in data.iterrows():
        if (team_id == ""):
            add_user(row["team_id"], row["idaltop"], row["rol"], i, amb)
        else:
            add_user(team_id, row["idaltop"], row["rol"], i, amb)
    
    
    asignar_permisos_task(data["idaltop"].astype(str).tolist(),mycursor)
    input("Enter para continuar...")


def asignar_permisos_task(data:list,mycursor:MySQLCursor):
    sql = open("Archivos/permisos.sql","r")
    ids = ",".join(data)
    file_sql = sql.read().replace("{idaltops}",ids)
    mycursor.execute(file_sql,multi=True)
    sql.close()


def buscarCuenta(account, country, mycursor):
    
    res = Db.ejecutarConsulta(
        f"Select name from custos.accounts where id = {account} and country_config_id = {country} and status = 1 and active_status = 1", mycursor)
    if (len(res) == 0):
        print(f"No se encuentra la cuenta {account} para el pais {country}")
        sys.exit()
    else:
        print(f"La cuenta es {res[0][0]} para el pais {country}")
        res = input("¿Desea continuar? (s/n) > ")
        if (res != "s"):
            sys.exit()


def buscar_cuenta_nombre(nombre, country, mycursor)->int:
    res = Db.ejecutarConsulta(
        f'Select id,name from custos.accounts where name like "%{nombre}%" and country_config_id = {country} and status = 1 and active_status = 1', mycursor)
    if (len(res) == 0):
        res = Db.ejecutarConsulta(
        f"Select id,name from custos.accounts where name like '%{nombre[:3]}%' and country_config_id = {country} and status = 1 and active_status = 1", mycursor)
        if (len(res) == 0):
            print("La cuenta con nombre",nombre, "No se encuentra para el país indicado, por favor corroborar")
            return None
        for i in range(len(res)):
            print(f"{i+1}. id: {res[i][0]} nombre: {res[i][1]} ")
            while True:
                try:
                    cuenta = int(input("Seleccione la cuenta > "))
                    return res[cuenta-1][0]
                except Exception as _:
                    print("Selecciona bien :/")
                    continue
    else:
        if (len(res)== 1):
            print(
            f"La cuenta es {res[0][0]} con nombre {res[0][1]} para el pais {country}")
            en = input("¿Desea continuar? (s/n) > ")
            if (en == "s"):
                return res[0][0]
        if (len(res) > 1):
            for i in range(len(res)):
                print(f"{i+1}. id: {res[i][0]} nombre: {res[i][1]} ")
            while True:
                try:
                    cuenta = int(input("Seleccione la cuenta > "))
                    return res[cuenta-1][0]
                except Exception as _:
                    print("Selecciona bien :/")
                    continue
        print(
            f"La cuenta es {res[0][0]} con nombre {res[0][1]} para el pais {country}")
        en = input("¿Desea continuar? (s/n) > ")
        if (en == "s"):
            return res[0][0]
        else:
            exit()


def ingreso_cuenta():
    while True:
        try:
            account = int(input("   Ingrese el Id de la cuenta > "))
            return account
        except Exception as _:
            print(INGRESO_VALIDO)
            continue


def ingreso_pais():
    while True:
        try:
            pais = int(input("   Ingrese el id del pais > "))
            return pais
        except Exception as _:
            print(INGRESO_VALIDO)
            continue


def select_ambiente() -> Tuple[str,MySQLCursor]:
    global db,mycursor
    while True:
        try:
            amb = input("""   
                1. Produccion
                2. Dev

            Ingrese el ambiene  > """)
            if (amb not in ("1", "2")):
                print("ese no es compa")
                continue
            res = input(
                f"Está seguro que desea ingresar a {AMB[amb]['name']} ? (s/n) > ")
            if (res == "s"):
                db, mycursor = Db.iniciarConexion(AMB[amb]['name'])
                return amb,mycursor
            else:
                amb = 0
                continue
        except ValueError as e:
            print(INGRESO_VALIDO)
        except InterfaceError as e:
            print("\n",CONEXION_INVALIDA,e.args[1])
            input(PRESIONE)


def ingrese_team_id(amb):
    while True:
        team_id = input("   Ingrese el team_id > ")
        en = input(
            f"¿ está seguro que el team_id es '{team_id}'? (s: Sí/otro letra: No) > ")
        if (en == "s"):
            team = buscarTeam(team_id, amb)
            if not team:
                return team_id
            else:
                print(
                    f"Se encontró que el team con team_id '{team_id}' ya existe\n")
                print(team)
                team_id = ""
                return None


def main(amb,mycursor:MySQLCursor):
    team_id = ""
    name = ""
    country = 0
    account = 0

    while name == "":
        country = ingreso_pais()
        account = buscar_cuenta_nombre(
            input("Ingresa el nombre de la cuenta> "), country ,mycursor)
        if not account:
            return
        if (name == ""):
            name = input("   Ingrese el nombre del team > ")
            en = input(
                f"¿ está seguro que el nombre del team es '{name}' ? (s: Sí/otro letra: No) > ")
            if (en == "s"):
                if (amb == "1"):
                    num = buscarName(name, account,mycursor, amb)
                    if num:
                        val = pd.DataFrame(
                            num, columns=["team_id", "name", "acount"])
                        print(
                            f"Se encontraron unas coincidencias con el nombre o el team ya existe para la cuenta {account}\n", val)
                        name = ""
                        break
            else:
                name = ""

        team_id = ingrese_team_id(amb)
        if (not team_id):
            return

    if (name != ""):
        buscarCuenta(account, country, mycursor)
        insertaTeam(team_id, name, country, account, amb,mycursor)
        while True:
            en = input("\n ¿Desea ingresar usuarios al team? (s: Sí/n: No) > ")
            print(
                    "\nAquí no es necesario indicar el team en el formato, pues todos van para el mismo team")
            if (en == "s"):
                new_en = input(FORMATO_USUARIOS)
                
                if (new_en == "s"):
                    insertarUsers(account, country,amb,mycursor,team_id)
                    return
                else:
                    os.system("xdg-open Archivos/UsuariosTeams.xlsx")
                
            elif (en == "n"):
                return
            else:
                print("esa no es compa")


def multiple_creation(amb,mycursor):
    data: pd.DataFrame = pd.read_excel(f"Archivos/create_multiple_teams.xlsx",engine='openpyxl')
    data.columns = ["team_name", "team_id", "account", "country_id"]
    data["account_id"] = [buscar_cuenta_nombre(row["account"], int(
        row["country_id"]), mycursor) for _, row in data.iterrows()]
    if (len(data[pd.isna(data["account_id"])]) > 0):
        print("No se encontraron las siguientes cuentas")
        print(data[pd.isna(data["account_id"])])
        return
    try:
        map(int, data["country_id"])
    except Exception as _:
        print("Los campos del pais deben ser numéricos")

    data["check_name"] = data[["team_name", "account_id"]].apply(
        lambda row: buscarName(row["team_name"], row["account_id"],mycursor, amb), axis=1)
    df_no_team_name = data.loc[data["check_name"]]
    if len(df_no_team_name) > 0:
        print("\nlos siguientes nombres para los teams en el pais indicado ya existen\n")
        print(df_no_team_name)
        exit()
    users:pd.DataFrame = insert_multiple_teams_users(data["country_id"][0],amb,mycursor)

    for _,row in data.iterrows():
        if not buscarTeam(row["team_id"], amb):
            user_team = users.copy().loc[users["team_id"] == row["team_id"]]
            user_team.reset_index(inplace=True)
            print(user_team)
            buscarCuenta(row["account_id"], row["country_id"],mycursor )
            print(f"\nSe va a crear el team: {row['team_id']}\n")
            en = input("\n¿Desea continuar? (s/n)> ")
            if (en != "s"):
                continue
            insertaTeam(row["team_id"], row["team_name"],
                     row["country_id"], row["account_id"], amb,mycursor)
            for i, row in user_team.iterrows():
                add_user(row["team_id"], row["idaltop"], row["rol"], i, amb)
        else:
            print(f"El team_id ya existe {row['team_id']}")
            exit()
    # for _, row in data.iterrows():
    #     insertaTeam(row["team_id"], row["team_name"],
    #                 row["country_id"], row["account_id"], amb)


def insert_multiple_teams_users(contry_id, amb,mycursor) -> pd.DataFrame:
    print("\n\tIngrese para cada team los usuarios con sus respectivos team_id")
    confirm_excel("UsuariosTeams.xlsx")
    df_user = pd.read_excel(f"{PATH}/Archivos/UsuariosTeams.xlsx",engine='openpyxl')
    df_user.columns = ["email", "rol", "team_id"]
    df_user = buscarUsuarios(df_user, False, contry_id, amb,mycursor)
    return df_user


def confirm_excel(archivo: str):
    while True:
        en = input(f"\n ¿Ya llenó el archivo '{archivo}'? (s/n)>")
        if (en == 's'):
            break
        else:
            os.system(f"xdg-open {PATH}/Archivos/{archivo}")


def mostrar_team(amb,cuenta=0):
    print("\n Por favor ingrese el nombre para del team para sacar el team_id")
    while True:
        nombre = input("Ingrese nombre del team > ")
        print(
            f"++++++++++++++ Teams con nombre '{nombre}' +++++++++++++++++++++++\n")
        print(json.dumps(getTeam_with_name(amb, nombre,cuenta), indent=5))
        val = input("\n¿Encontró el team que buscaba? (s/n) >")
        if (val == "s"):
            break

def delete_teams(amb):
    data: pd.DataFrame = pd.read_excel(f"Archivos/DeleteTeams.xlsx")
    data["validate"] = data["team_id"].apply(
        lambda row: buscarTeam(row['team_id'],amb))
    if (len(data[pd.isna(data["validate"])])>0):
        print("No existen los siguientes team_id")
        print(data[pd.isna(data["validate"])].drop("validate",1))
        exit()
    print("\n",data.drop("validate",1))
    en = input("\t\n ¿Seguro que desea eliminar los teams? (s/n) >")
    if en == "s":
        deleteTeam(amb,data["team_id"])


def menu():
    amb, mycursor = select_ambiente()
    while True:

        print("""
        ********************************************************
                            ¿Qué deseas hacer?
        ********************************************************                
        1. Crear un team
        2. Crear varios teams
        3. Ingresar usuarios a un team
        4. Ingresar usuarios para teams diferentes
        5. Buscar team(s) por nombre o parecidos
        6. Buscar team por cuenta
        7. Buscar teams de un usuario
        8. Buscar usuarios de un team
        9. Eliminar team
        10. Eliminar multiplues teams
        11. Eliminar usuario de team
        12. Cambiar campo de un team

        Puedes salir de la aplicación presionando 'Ctrl+c' en cualquier momento

          ------para cambiar de ambiente ingrese 'cambio'------
        *********************************************************
        """)
        en = input("Ingrese valor > ")
        if en == "1":
            main(amb,mycursor)
        elif en == "2":
            new_en = input(
                "\n¿Ya llenó el formato de excel 'create_multiple_teams.xlsx'? (s/n) > ")
            if (new_en == "s"):
                multiple_creation(amb, mycursor)
            else:
                os.system(f"xdg-open {PATH}/Archivos/create_multiple_teams.xlsx")
        elif en == "3":
            new_en = input(
                "\n¿Ya llenó el formato de excel 'UsuariosTeams.xlsx'? (s/n) > ")
            print(
                "\nAquí no es necesario indicar el team en el formato, pues todos van para el mismo team")
            if (new_en == "s"):
                pais = ingreso_pais()
                cuenta = buscar_cuenta_nombre(input(INGRESO_CUENTA), pais, mycursor)
                if not cuenta: 
                    input(PRESIONE)
                    continue
                mostrar_team(amb,cuenta)
                team = buscarTeam(input(INGRESE_TEAM_ID), amb)
                if (not team):
                    continue
                insertarUsers(cuenta, pais, amb,mycursor, team["team_id"])
            else:
                os.system(f"xdg-open {PATH}/Archivos/UsuariosTeams.xlsx")
        elif en == "4":
            new_en = input(FORMATO_USUARIOS)
            print(
                "\nAquí es necesario indicar el team en el formato, pues todos van para teams diferentes")
            if (new_en == "s"):
                # pais = ingreso_pais()
                # cuenta = buscar_cuenta_nombre(input(INGRESO_CUENTA), pais, amb)
                insertarUsers(cuenta, pais, amb,mycursor)
            else:
                os.system(f"xdg-open {PATH}/Archivos/UsuariosTeams.xlsx")
        elif en == "5":
            nombre = input("Ingrese nombre del team > ")
            print(
                f"++++++++++++++ Teams con nombre '{nombre}' +++++++++++++++++++++++\n")
            print(json.dumps(getTeam_with_name(amb, nombre), indent=5))
            input(PRESIONE)
        elif en == "6":
            cuenta = buscar_cuenta_nombre(
                input(INGRESO_CUENTA), ingreso_pais(), mycursor)
            if not cuenta:
                input(PRESIONE)
                continue
            print(
                f"++++++++++++++ Teams para la cuenta {cuenta} +++++++++++++++++++++++\n")
            print(json.dumps(getTeam_by_account(amb, cuenta), indent=5))
            input(PRESIONE)
        elif en == "7":
            usuario = input("Ingrese usuario > ")
            try:
                usuario = int(usuario)
                print(
                    f"++++++++++++++ Teams para el usuario {usuario} +++++++++++++++++++++++\n")
                getTeamUser(amb, usuario)
                input(PRESIONE)
            except Exception as _:
                print(
                    f"++++++++++++++ Teams para el usuario {usuario} +++++++++++++++++++++++\n")
                getTeamUser(amb, usuario,mycursor)
                input(PRESIONE)
        elif en == "8":
            mostrar_team(amb)
            team_id = buscarTeam(input(INGRESE_TEAM_ID), amb)
            print(
                f"++++++++++++++ Usuarios del team con tema_id '{team_id['team_id']}' +++++++++++++++++++++++\n")
            getUsersTeam(amb, team_id["team_id"])
            input(PRESIONE)
        elif en == "9":
            mostrar_team(amb)
            team_id = buscarTeam(input(INGRESE_TEAM_ID), amb)
            if team_id:
                deleteTeam(amb, team_id["team_id"])
        elif en == "10":
            confirm_excel("DeleteTeams.xlsx")
            delete_teams(amb)
            input(PRESIONE)
        elif en == "11":
            new_en = input(
                "\n¿Ya llenó el formato de excel 'DeleteUsersTeam.xlsx'? (s/n) > ")
            if (new_en == "s"):
                mostrar_team(amb)
                team_id = buscarTeam(input(INGRESE_TEAM_ID), amb)
                deleteTeamUser(amb, team_id["team_id"], ingreso_pais(),mycursor)
            else:
                os.system(f"xdg-open {PATH}/Archivos/DeleteUsersTeam.xlsx")
        elif en == "12":
            mostrar_team(amb)
            team_id = buscarTeam(input(INGRESE_TEAM_ID), amb)["team_id"]
            if team_id:
                camb = input(f"""
                    ¿Qué deseas cambiar del team '{team_id}'
                        1. Nombre
                        2. Pais
                        3. cuenta
                    
                    ingrese valor >""")
                new_valor = input(
                    f"Ingrese el nuevo valor para el campo '{CAMBIO[camb]}' para el team '{team_id}' >")
                cambiar_value_team(team_id, camb, new_valor, amb)
        elif en == "12":
            break

        elif en.lower() == "cambio":
            amb = select_ambiente()


if __name__ == "__main__":
    """
        Aqui se ejecuta el menú con todas las implementaciones para un TEAM
    """
    try:
        menu()
        Db.cerrarConexion(db,mycursor)
        
    except KeyboardInterrupt as _:
        print("\nBye :)")
        Db.cerrarConexion(db,mycursor)
